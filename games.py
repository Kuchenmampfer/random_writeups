from random import randint, shuffle
import discord
import numpy as np
from discord.ext import commands
from asyncio import TimeoutError
from datetime import timedelta, datetime

from cogs.utils import functions


class Games(commands.Cog):
    """random Games"""

    def __init__(self, bot):
        self.bot = bot

    async def update_elo(self, winner_id, loser_id, was_draw):
        get_elo = '''
                   SELECT vier_gewinnt_elo
                   FROM DiscordMembers
                   WHERE member_id = $1
                   '''
        update_elo = '''
                     UPDATE DiscordMembers
                     SET vier_gewinnt_elo = vier_gewinnt_elo + $2
                     WHERE member_id = $1
                     '''
        async with self.bot.pool.acquire() as conn:
            record = await conn.fetchrow(get_elo, winner_id)
            winner_elo = record[0]
            record = await conn.fetchrow(get_elo, loser_id)
            loser_elo = record[0]
            expection = 1 / (1 + 10 ** ((loser_elo - winner_elo) / 50))
            if was_draw:
                score = 0
            else:
                score = 1
            change = int(round(5 * (score - expection)))
            await conn.execute(update_elo, winner_id, change)
            await conn.execute(update_elo, loser_id, - change)

    async def get_opponent(self, ctx, out, opponent, game):
        if opponent == 'anyone':
            await out.send(f'Wer will gegen **{ctx.author.display_name}** {game} spielen?')

            def check_wer(message):
                ans = message.content
                return ans.lower() == 'ich'

            try:
                answer = await self.bot.wait_for('message', check=check_wer, timeout=150.0)
            except TimeoutError:
                await out.send('Sorry, anscheinend will niemand mit dir spielen :basket:')
                return
            opponent = answer.author
        else:
            try:
                converter = commands.MemberConverter()
                opponent = await converter.convert(ctx, opponent)
            except BaseException:
                raise commands.BadArgument
            await out.send(f'{opponent.mention} möchtest du gegen '
                           f'**{ctx.author.display_name}** {game} spielen?')

            def check_bool(message):
                ans = message.content
                return ans.lower() in ['j', 'ja', 'yes', 'n', 'nein', 'no'] and message.author == opponent

            try:
                answer = await self.bot.wait_for('message', check=check_bool, timeout=60.0)
            except TimeoutError:
                await out.send(f'{opponent.display_name} antwortet nicht, {game} fällt aus :pensive:')
                return None
            answer = answer.content
            if answer.lower() in ['n', 'nein', 'no']:
                await out.send('Ok, dann spielen wir nicht :thumbsup:')
                return None
        return opponent

    @commands.command(name='tictactoe', aliases=['tiktakto', 'ticktackto'])
    async def tictactoe(self, ctx, opponent='anyone'):
        out = await functions.channel_check(self.bot, ctx, 'Tictactoe')
        opponent = await self.get_opponent(ctx, out, opponent, 'Tictactoe')
        if opponent is None:
            return
        players = [ctx.author, opponent]
        game = np.zeros((3, 3), dtype='i1')
        title = f'Tictactoe :o: {ctx.author.display_name} :crossed_swords: ' \
                f'{opponent.display_name} :x:'
        game_runs = True
        has_the_move = randint(0, 1)
        columns_dict = {'a': 0, 'b': 1, 'c': 2}
        symbols_dict = {0: self.bot.emotes['blank'], 1: ':o:', 2: ':x:'}
        was_draw = False

        def check_field(message):
            return message.content in ['a1', 'a2', 'a3', 'b1', 'b2', 'b3', 'c1', 'c2', 'c3', 'end'] \
                   and message.author == players[has_the_move]

        while game_runs:
            field_game = f'{self.bot.emotes["blank"]}:regional_indicator_a:' \
                         f':regional_indicator_b::regional_indicator_c:'
            embed = discord.Embed(color=discord.Color.orange())
            for i in range(3):
                field_game += '\n' + self.bot.emotes[i + 1]
                for field in game[i]:
                    field_game += symbols_dict[field]
            embed.add_field(name=title, value=field_game, inline=False)
            name = f'{players[has_the_move].display_name} ist am Zug.' \
                   f'Welches Feld möchtest du besetzen?'
            value = 'Beispiel: `a1`'
            embed.add_field(name=name, value=value)
            message_old = await out.send(embed=embed)
            try:
                position = await self.bot.wait_for('message', check=check_field, timeout=60.0)
            except TimeoutError:
                await out.send(f'{players[has_the_move].display_name} du bist zu lahm. \n'
                               f'{players[abs(has_the_move - 1)].display_name} hat gewonnen :partying_face:')
                return

            position = position.content
            if position == 'end':
                game_runs = False
                await message_old.delete(delay=0.2)
                has_the_move = abs(has_the_move - 1)
                continue

            if game[int(position[1]) - 1][columns_dict[position[0]]] == 0:
                game[int(position[1]) - 1][columns_dict[position[0]]] = has_the_move + 1
            else:
                await out.send('Dieses Feld ist leider schon besetzt')
                await message_old.delete(delay=0.2)
                continue

            row_min = np.ma.min(game, axis=1)
            row_max = np.ma.max(game, axis=1)
            column_min = np.ma.min(game, axis=0)
            column_max = np.ma.max(game, axis=0)
            diagonal_1 = []
            diagonal_2 = []
            won = False
            for i in range(3):
                row_equal = row_min[i] == row_max[i] and row_min[i] != 0
                column_equal = column_min[i] == column_max[i] and column_min[i] != 0
                if row_equal or column_equal:
                    game_runs = False
                    await message_old.delete(delay=0.2)
                    won = True
                diagonal_1.append(game[i, i])
                diagonal_2.append(game[i, 2 - i])
            diag_1_equal = min(diagonal_1) == max(diagonal_1) != 0
            diag_2_equal = min(diagonal_2) == max(diagonal_2) != 0
            if diag_1_equal or diag_2_equal or won:
                game_runs = False
                await message_old.delete(delay=0.2)
                continue
            await message_old.delete(delay=0.2)
            if np.count_nonzero(game) == 9:
                was_draw = True
                game_runs = False
                await message_old.delete(delay=0.2)
                continue

            has_the_move = abs(has_the_move - 1)

        field_game = f'{self.bot.emotes["blank"]}:regional_indicator_a:' \
                     f':regional_indicator_b::regional_indicator_c:'
        embed = discord.Embed(color=discord.Color.orange())
        for i in range(3):
            field_game += '\n' + self.bot.emotes[i + 1]
            for field in game[i]:
                field_game += symbols_dict[field]
        embed.add_field(name=title, value=field_game, inline=False)
        if was_draw:
            name = 'Och nee, ein Unentschieden :pensive:'
            value = f'{players[0].display_name} und {players[1].display_name}' \
                    ' sind offenbar beide gleich schlecht'
        else:
            name = f'{players[has_the_move].display_name} hat gewonnen.'
            value = 'Herzlichen Glückwunsch :partying_face:'
        embed.add_field(name=name, value=value)
        await out.send(embed=embed)
        async with self.bot.pool.acquire() as conn:
            await conn.execute('''
                               INSERT INTO DuelGamesResults(time, game, winner_id, loser_id, was_draw)
                               VALUES($1, $2, $3, $4, $5)
                               ''',
                               datetime.now(), 'tictactoe', players[has_the_move].id,
                               players[abs(has_the_move - 1)].id, was_draw
                               )

    @tictactoe.error
    async def tictactoe_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            await ctx.send('Bro, das ist kein zulässiger Gegner')

    @commands.command(name='viergewinnt', aliases=['vier-gewinnt', '4gewinnt'])
    async def vier_gewinnt(self, ctx, opponent='anyone'):
        out = await functions.channel_check(self.bot, ctx, 'vier gewinnt')
        opponent = await self.get_opponent(ctx, out, opponent, 'vier gewinnt')
        if opponent is None:
            return
        players = [ctx.author, opponent]
        shuffle(players)
        game = np.zeros((6, 7), dtype='i1')
        title = f'Vier gewinnt :red_circle: {players[0].display_name} :crossed_swords: ' \
                f'{players[1].display_name} :yellow_circle:'
        game_runs = True
        has_the_move = 0
        symbols_dict = {0: ':black_circle:', 2: ':red_circle:', 3: ':yellow_circle:'}
        moves = [[], []]
        times = [timedelta(minutes=4), timedelta(minutes=4)]
        message_game = None
        was_draw = False

        def check_field(message):
            return message.content in ['1', '2', '3', '4', '5', '6', '7', 'end'] \
                   and message.author == players[has_the_move]

        def last_zero(arr, axis, invalid_val=-1):
            mask = arr == 0
            val = arr.shape[axis] - np.flip(mask, axis=axis).argmax(axis=axis) - 1
            return np.where(mask.any(axis=axis), val, invalid_val)

        def create_game_field(embed):
            field_game = f'{self.bot.emotes[1]}{self.bot.emotes[2]}{self.bot.emotes[3]}' \
                         f'{self.bot.emotes[4]}{self.bot.emotes[5]}{self.bot.emotes[6]}' \
                         f'{self.bot.emotes[7]}'
            for i in range(6):
                field_game += '\n'
                for field in game[i]:
                    field_game += symbols_dict[field]
            embed.add_field(name=title, value=field_game, inline=False)
            h_name = 'Spielverlauf:'
            history = ''
            for i in range(2):
                history += f'{symbols_dict[i + 2]}: '
                for move in moves[i]:
                    history += self.bot.emotes[move]
                history += '\n'
            embed.add_field(name=h_name, value=history, inline=False)

        def check_four(array):
            strike = 0
            for field in array:
                if field == has_the_move + 2:
                    strike += 1
                else:
                    strike = 0
                if strike == 4:
                    return True
            return False

        while game_runs:
            embed = discord.Embed(color=discord.Color.orange())
            create_game_field(embed)
            name = f'{players[has_the_move].display_name} ist am Zug.'
            hours, remainder = divmod(times[has_the_move].total_seconds(), 3600)
            minutes, seconds = divmod(remainder, 60)
            value = f':timer: Verbleibende Bedenkzeit: ' \
                    f'**{int(minutes):>02}:{int(seconds):>02}**'
            embed.add_field(name=name, value=value)
            if message_game is None:
                message_game = await out.send(embed=embed)
            else:
                await message_game.edit(embed=embed)
            start_thinking = datetime.now()
            try:
                position = await self.bot.wait_for('message', check=check_field,
                                                   timeout=times[has_the_move].total_seconds())
            except TimeoutError:
                await out.send(f'{players[has_the_move].display_name} du bist zu lahm. \n'
                               f'{players[abs(has_the_move - 1)].display_name} hat gewonnen :partying_face:')
                embed = discord.Embed(color=discord.Color.orange())
                create_game_field(embed)
                name = f'{players[abs(has_the_move - 1)].display_name} hat gewonnen.'
                value = 'Herzlichen Glückwunsch :partying_face:'
                embed.add_field(name=name, value=value)
                await message_game.edit(embed=embed)
                return

            times[has_the_move] -= datetime.now() - start_thinking
            try:
                await position.delete()
            except discord.Forbidden:
                self.bot.logger.info(f'Auf {ctx.guild.name} '
                                     f'fehlt mir die Berechtigung, Nachrichten zu löschen.')
            position = position.content
            if position == 'end':
                game_runs = False
                has_the_move = abs(has_the_move - 1)
                continue

            position = int(position) - 1
            row = last_zero(game, 0)[position]
            if row == -1:
                await out.send('Diese Spalte ist leider schon besetzt')
                continue
            else:
                game[row, position] = has_the_move + 2
                moves[has_the_move].append(position + 1)
            vertical = np.prod(game[row:row + 4, position]) in [16, 81]
            horizontal = check_four(game[row, :])
            diagonal_1 = check_four(np.diagonal(game, position - row))
            diagonal_2 = check_four(np.diagonal(np.fliplr(game), 6 - position - row))

            if vertical or horizontal or diagonal_1 or diagonal_2:
                game_runs = False
                continue
            if np.count_nonzero(game) == 42:
                was_draw = True
                game_runs = False
                continue

            has_the_move = abs(has_the_move - 1)

        embed = discord.Embed(color=discord.Color.orange())
        create_game_field(embed)
        if was_draw:
            name = 'Och nee, ein Unentschieden :pensive:'
            value = f'{players[0].display_name} und {players[1].display_name}' \
                    ' sind offenbar beide gleich schlecht'
        else:
            name = f'{players[has_the_move].display_name} hat gewonnen.'
            value = 'Herzlichen Glückwunsch :partying_face:'
        embed.add_field(name=name, value=value)
        await message_game.edit(embed=embed)
        async with self.bot.pool.acquire() as conn:
            await conn.execute('''
                               INSERT INTO DuelGamesResults(time, game, winner_id, loser_id, was_draw)
                               VALUES($1, $2, $3, $4, $5)
                               ''',
                               datetime.now(), 'vier_gewinnt', players[has_the_move].id,
                               players[abs(has_the_move - 1)].id, was_draw
                               )
        await self.update_elo(players[has_the_move].id, players[abs(has_the_move - 1)].id, was_draw)

    @vier_gewinnt.error
    async def vier_gewinnt_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            await ctx.send('Bro, das ist kein zulässiger Gegner')

    # @commands.command(name='calc_elo')
    # @commands.is_owner()
    # async def calc_elo(self, ctx):
    #     async with self.bot.pool.acquire() as conn:
    #         records = await conn.fetch('''
    #                                    SELECT winner_id, loser_id, was_draw
    #                                    FROM DuelGamesResults
    #                                    WHERE game = 'vier_gewinnt'
    #                                    ''')
    #     for record in records:
    #         await self.update_elo(record[0], record[1], record[2])
    #     await ctx.send(f'{len(records)} Elos geupdated :white_check_mark:')


def setup(bot):
    bot.add_cog(Games(bot))
